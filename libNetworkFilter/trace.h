#pragma once

#ifdef _DEBUG

static void DbgPrintA(const char * format, ...)
{
    //#ifdef _DEBUG
    char buff[4096] = { 0 };
    va_list ap;
    va_start(ap, format);
    _vsnprintf_s(buff, _countof(buff), format, ap);
    va_end(ap);

    OutputDebugStringA(buff);
    //std::cout << buff;
}

static void DbgPrintW(const wchar_t * format, ...)
{
    //#ifdef _DEBUG
    wchar_t buff[4096] = { 0 };
    va_list ap;
    va_start(ap, format);
    _vsnwprintf_s(buff, _countof(buff), format, ap);
    va_end(ap);

    OutputDebugStringW(buff);
    //std::wcout << buff;
}

#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) L ## #x
#define LINE_STRING STRINGIZE(__LINE__)
#define STR(x) STR2(x)
#define STR2(x) L ## x
#define FUNC STR(__FUNCTION__)
#define PrintPrefix L"[LSP][" FUNC L":" LINE_STRING L"] "

#endif

#ifdef _DEBUG

#define DebugPrint(...) DbgPrintW(PrintPrefix __VA_ARGS__)
#else

#define DebugPrint(...)

#endif