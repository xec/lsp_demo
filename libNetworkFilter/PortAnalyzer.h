#pragma once

#include <set>
#include <WINDOWS.H>

namespace LNF
{
    class PortAnalyzer
    {
    public:
        PortAnalyzer();
        ~PortAnalyzer();
        friend class Analyzer;

        void clear();
        void addImportantPort(const short port);
        void loadDefaultImportantPort();

    protected:
        bool isImportantPort(const short port);

    private:
        CRITICAL_SECTION cs_lock_;
        std::set<short> ports_;
    };
}