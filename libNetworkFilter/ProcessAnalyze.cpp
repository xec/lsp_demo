#include <WINDOWS.H>
#include <Psapi.h>
#include <string>
#include "ProcessAnalyze.h"
#include <atlbase.h>
#include <atlfile.h>
#include <imagehlp.h>

#pragma comment(lib, "Dbghelp.lib")
#pragma comment(lib, "psapi.lib")

namespace LNF
{
    ProcessAnalyze::ProcessAnalyze(long pid) :
        ProcessId_(pid)
    {}


    ProcessAnalyze::~ProcessAnalyze()
    {}

    static std::wstring ConvertDosDeviceNameToDriveName(std::wstring strDosName)
    {
        std::wstring strResult = _T("");
        WCHAR szText[512] = { 0 };
        WCHAR szDrives[512] = { 0 };
        WCHAR szDriveLetter[3] = { 0 };

        GetLogicalDriveStrings(512, szDrives);

        WCHAR* psz = szDrives;

        while (*psz != NULL) {
            lstrcpyn(szDriveLetter, psz, 3);
            QueryDosDevice(szDriveLetter, szText, 512);

            if (strDosName.substr(0, lstrlen(szText)) == szText) {
                strResult = strDosName;
                strResult.replace(0, lstrlen(szText), szDriveLetter);
                break;
            }

            psz += lstrlen(psz) + 1;
        }

        return strResult;
    }

    bool ProcessAnalyze::isConsoleProcess()
    {
        return imageNtHeaders_ && imageNtHeaders_->OptionalHeader.Subsystem == IMAGE_SUBSYSTEM_WINDOWS_CUI;
    }

    bool ProcessAnalyze::analyze()
    {
        bool bRet = false;
        ATL::CHandle hProcess;
        TCHAR  processImageFileName[MAX_PATH] = { 0 };

        hProcess.Attach(OpenProcess(PROCESS_ALL_ACCESS, FALSE, ProcessId_));
        if (!hProcess) {
            goto cleanup;
        }

        GetProcessImageFileName(hProcess, processImageFileName, MAX_PATH);
        ProcessImageFilePath_ = ConvertDosDeviceNameToDriveName(processImageFileName);

        bRet = analyzeFile();

    cleanup:
        return bRet;
    }

    bool ProcessAnalyze::analyzeFile()
    {
        HRESULT hr = S_OK;
        
        PVOID OldValue;
        Wow64DisableWow64FsRedirection(&OldValue);

        hr = file.Create(ProcessImageFilePath_.c_str(),
                         GENERIC_READ,
                         FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
                         OPEN_EXISTING);
        if (FAILED(hr))
            goto cleanup;

        hr = mapping.MapFile(file);
        if (FAILED(hr))
            goto cleanup;

        imageNtHeaders_ = ImageNtHeader(mapping);

    cleanup:
        Wow64RevertWow64FsRedirection(OldValue);
        return S_OK == hr;
    }

    std::wstring ProcessAnalyze::getProcessPath() const
    {
        return ProcessImageFilePath_;
    }

}