#include <windows.h>
#include "TrustList.h"
namespace LNF
{
    TrustList* TrustList::trust_instnace_ = NULL;

    TrustList::TrustList()
    {
        InitializeCriticalSection(&cslock_);
    }


    TrustList::~TrustList()
    {
        DeleteCriticalSection(&cslock_);
    }

    TrustList* TrustList::Instance()
    {
        if (!trust_instnace_) {
            trust_instnace_ = new TrustList;
        }
        return trust_instnace_;
    }

    void TrustList::addTrustProcess(long pid)
    {
        EnterCriticalSection(&cslock_);
        trust_process_.insert(trust_process_.end(), pid);
        LeaveCriticalSection(&cslock_);
    }

    bool TrustList::isTrustProcess(long pid)
    {
        bool bRet = false;

        EnterCriticalSection(&cslock_);
        auto ite = trust_process_.find(pid);
        bRet = ite != trust_process_.end();
        LeaveCriticalSection(&cslock_);

        return bRet;
    }
}