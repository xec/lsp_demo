#include <rpc.h>
#include "RpcServer.h"
#include "../RpcInterface/Interface_h.h"
#include <atlbase.h>
#include <atlsecurity.h>
#include <string>
#include <boost/format.hpp>
#include "trace.h"

#pragma comment(lib, "Rpcrt4")

namespace LNF
{
    RpcServer* RpcServer::instance_ = NULL;

    RpcServer::RpcServer()
    {

    }

    RpcServer::~RpcServer()
    {

    }

    RpcServer* RpcServer::Instance()
    {
        if (!instance_) {
            instance_ = new RpcServer;
        }
        return instance_;
    }

    bool RpcServer::Initialize()
    {
        bool bRet = false;
        ///<    RPC服务器
        do {
            ATL::CSecurityAttributes sa;
            ATL::CSecurityDesc sd;
            ATL::CDacl dacl;
            ATL::CSid  sidEveryone, sidUsers;
            ATL::CAccessToken token;

            sidEveryone.LoadAccount(L"Everyone");
            sidUsers.LoadAccount(L"Users");

            token.GetDefaultDacl(&dacl);
            dacl.AddAllowedAce(ATL::Sids::Guests(), GENERIC_ALL);
            dacl.AddAllowedAce(ATL::Sids::Admins(), GENERIC_ALL);
            dacl.AddAllowedAce(ATL::Sids::Local(), GENERIC_ALL);
            dacl.AddAllowedAce(sidEveryone, GENERIC_ALL);
            dacl.AddAllowedAce(sidUsers, GENERIC_ALL);

            sd.SetDacl(dacl);

            RPC_STATUS status = RpcServerUseProtseqEp(reinterpret_cast<RPC_WSTR>(L"ncacn_np"),
                                                      RPC_C_PROTSEQ_MAX_REQS_DEFAULT,
                                                      reinterpret_cast<RPC_WSTR>(L"\\pipe\\{AAAF0E1E-18C1-4351-9483-52D7E5076B40}"),
                                                      (void*)sd.GetPSECURITY_DESCRIPTOR());
            if (RPC_S_OK != status) {
                DebugPrint(L"RpcServerUseProtseqEpW error: %08X\n", status);
                break;
            }

            // status = RpcServerRegisterIf(IInterface_v1_0_s_ifspec, nullptr, nullptr);
            status = RpcServerRegisterIfEx(IInterface_v1_0_s_ifspec, // Interface to register.   
                                           NULL,                  // Use the MIDL generated entry-point vector.   
                                           NULL,
                                           RPC_IF_ALLOW_CALLBACKS_WITH_NO_AUTH,
                                           RPC_C_PROTSEQ_MAX_REQS_DEFAULT,
                                           NULL);
            if (RPC_S_OK != status) {
                DebugPrint(L"RpcServerRegisterIf error: %08X\n", status);
                break;
            }

            ///<    非阻塞监听
            status = RpcServerListen(10, 10, TRUE);
            if (RPC_S_OK != status) {
                DebugPrint(L"RpcServerListen error: %08X\n", status);
                break;
            }

            bRet = true;
        } while (false);

        return bRet;
    }

    void RpcServer::Close()
    {
        ///<    关闭RPC服务器
        if (RPC_S_OK == ::RpcMgmtStopServerListening(nullptr)) {
            ::RpcServerUnregisterIf(nullptr, nullptr, FALSE);
        }
    }

    void RpcServer::RegisterConnectEvent(RpcConnectEvent event)
    {
        ConnectEvents_.push_back(event);
    }

    void RpcServer::RegisterSendEvent(RpcSendEvent event)
    {
        SendEvents_.push_back(event);
    }

    void RpcServer::RegisterRecvEvent(RpcRecvEvent event)
    {
        RecvEvents_.push_back(event);
    }

	void RpcServer::RegisterBindEvent(RpcBindEvent event)
	{
		BindEvents_.push_back(event);
	}

    bool RpcServer::OnConnectEvent(const std::wstring& filepath, unsigned long pid, const std::wstring& ip, unsigned short port)
    {
        for each(const RpcConnectEvent& event in ConnectEvents_)
        {
            if (!event(filepath, pid, ip, port))
                return false;
        }

        return true;
    }

    bool RpcServer::OnSendEvent(const std::wstring& filepath, unsigned long pid, unsigned char* buff, long bufsize)
    {
        for each(const RpcSendEvent& event in SendEvents_)
        {
            if (!event(filepath, pid, buff, bufsize))
                return false;
        }
        return true;
    }

    bool RpcServer::OnRecvEvent(const std::wstring& filepath, unsigned long pid, unsigned char* buff, long bufsize)
    {
        for each(const RpcRecvEvent& event in RecvEvents_)
        {
            if (!event(filepath, pid, buff, bufsize))
                return false;
        }
        return true;
    }

	bool RpcServer::OnBindEvent(const std::wstring& filepath, unsigned long pid, unsigned short port)
	{
		for each(const RpcBindEvent& event in BindEvents_)
		{
			if (!event(filepath, pid, port))
				return false;
		}
		return true;
	}


}

boolean checkConnectAction(handle_t IDL_handle, BSTR filepath, ULONG pid, BSTR ip, unsigned short port)
{
    std::wstring wip = (wchar_t*)ip;
    return LNF::RpcServer::Instance()->OnConnectEvent(filepath, pid, wip, port);
}

boolean checkSendAction(handle_t IDL_handle, BSTR filepath, ULONG pid, SAFEARRAY * psa)
{
    boolean bRet = true;
    unsigned char* lockedData = NULL;
    long offset = 0, bufsize = 0;
    SafeArrayGetLBound(psa, 1, &offset);
    SafeArrayGetUBound(psa, 1, &bufsize);

    SafeArrayAccessData(psa, (void**)&lockedData);

    bRet = LNF::RpcServer::Instance()->OnSendEvent(filepath, pid, lockedData, bufsize);
    SafeArrayUnaccessData(psa);


    return bRet;
}

boolean checkRecvAction(handle_t IDL_handle, BSTR filepath, ULONG pid, SAFEARRAY * psa)
{
    boolean bRet = true;
    unsigned char* lockedData = NULL;
    long offset = 0, bufsize = 0;
    SafeArrayGetLBound(psa, 1, &offset);
    SafeArrayGetUBound(psa, 1, &bufsize);

    SafeArrayAccessData(psa, (void**)&lockedData);

    bRet = LNF::RpcServer::Instance()->OnRecvEvent(filepath, pid, lockedData, bufsize);
    SafeArrayUnaccessData(psa);


    return bRet;
}

boolean checkListenAction(handle_t IDL_handle, BSTR filepath, ULONG pid, unsigned short port) {
	return LNF::RpcServer::Instance()->OnBindEvent(filepath, pid, port);
}

void __RPC_FAR* __RPC_USER midl_user_allocate(size_t len)
{
    return(malloc(len));
}

void __RPC_USER midl_user_free(void __RPC_FAR *ptr)
{
    free(ptr);
}
