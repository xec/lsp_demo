#pragma once

#include <string>
#include <set>

namespace LNF
{
    class TrustList
    {
    public:
        TrustList();
        ~TrustList();

        static TrustList* Instance();

        void addTrustProcess(long pid);
        void addTrustFile(const std::wstring& path);

        bool isTrustProcess(long pid);

    private:
        CRITICAL_SECTION   cslock_;
        static TrustList* trust_instnace_;
        std::set<long>  trust_process_;
    };
}
