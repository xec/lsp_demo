#include "ProtocolAnalyze.h"
#include <memory.h>

namespace LNF
{
    ProtocolAnalyze::ProtocolAnalyze()
    {
        setSocks5AnalysisEnable(true);
    }
    
    ProtocolAnalyze::~ProtocolAnalyze()
    {}

    void ProtocolAnalyze::setSocks5AnalysisEnable(bool bEnable)
    {
        analysis_socks5_ = bEnable;
    }

    bool ProtocolAnalyze::getSosks5AnalysisEnable() const
    {
        return analysis_socks5_;
    }

    bool ProtocolAnalyze::isSafeDataForRecv(const char* buff, long bufsize)
    {
        bool bRet = true;

        if (getSosks5AnalysisEnable()) {
            bRet = !IsSocks5Packet(buff, bufsize);
            if (!bRet)
                goto FINAL;
        }

    FINAL:

        return bRet;
    }

    bool ProtocolAnalyze::isSafeDataToSend(const char* buff, long bufsize)
    {
        bool bRet = true;

        if (getSosks5AnalysisEnable()) {
            bRet = !IsSocks5Packet(buff, bufsize);
            if (!bRet)
                goto FINAL;
        }

    FINAL:

        return bRet;
    }
    
    bool ProtocolAnalyze::IsSocks5Packet(const char* buff, long bufsize)
    {
        unsigned char signature[] = { 0x05, 0x01, 0x00 };
        
        if (bufsize != sizeof(signature))
            return false;

        return 0 == memcmp(buff, signature, bufsize);
    }

}