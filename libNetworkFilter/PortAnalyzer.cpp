#include "PortAnalyzer.h"

namespace LNF
{
    PortAnalyzer::PortAnalyzer()
    {
        InitializeCriticalSection(&cs_lock_);
    }


    PortAnalyzer::~PortAnalyzer()
    {
        DeleteCriticalSection(&cs_lock_);
    }

    void PortAnalyzer::addImportantPort(const short port)
    {
        EnterCriticalSection(&cs_lock_);
        ports_.insert(ports_.end(), port);
        LeaveCriticalSection(&cs_lock_);
    }

    void PortAnalyzer::loadDefaultImportantPort()
    {
        addImportantPort(21);  // FTP port
        addImportantPort(22);  // SSH port
        addImportantPort(23);  // Telnet port
        addImportantPort(3389); // RDP port
    }

    bool PortAnalyzer::isImportantPort(const short port)
    {
        bool bRet = false;
        EnterCriticalSection(&cs_lock_);
        auto ite = ports_.find(port);
        bRet =  ite != ports_.end();
        LeaveCriticalSection(&cs_lock_);
        return bRet;
    }

    void PortAnalyzer::clear()
    {
        EnterCriticalSection(&cs_lock_);
        ports_.clear();
        LeaveCriticalSection(&cs_lock_);
    }

}