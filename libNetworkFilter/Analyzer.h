#pragma once

#include <string>
#include "PortAnalyzer.h"
#include "ProtocolAnalyze.h"
#include "FilesystemAnalyze.h"

namespace LNF
{
    class Analyzer
    {
    public:
        Analyzer();
        ~Analyzer();

        static Analyzer& Instance();

        bool isSafeConnect(long pid, const std::wstring& ip, short port);
        bool isSafeSend(long pid, const char* buff, const long bufsize);
        bool isSafeRecv(long pid, const char* buff, const long bufsize);

        PortAnalyzer& port() { return port_; }
        ProtocolAnalyze& protocol() { return protocol_; }
        FilesystemAnalyze& filesystem() { return filesys_; }

    private:
        PortAnalyzer port_;
        ProtocolAnalyze protocol_;
        FilesystemAnalyze filesys_;
    };
}
