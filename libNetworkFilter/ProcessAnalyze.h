#pragma once
#include <Windows.h>
#include <atlbase.h>
#include <atlfile.h>
#include <boost/noncopyable.hpp>

namespace LNF
{
    class ProcessAnalyze : 
        public boost::noncopyable
    {
    public:
        ProcessAnalyze(long pid);
        ~ProcessAnalyze();

        bool analyze();
        bool isConsoleProcess();
        std::wstring getProcessPath() const;

    protected:
        bool analyzeFile();

    private:
        long ProcessId_;
        std::wstring ProcessImageFilePath_;
        PIMAGE_NT_HEADERS imageNtHeaders_;
        ATL::CAtlFile file;
        ATL::CAtlFileMapping<void> mapping;
    };

}