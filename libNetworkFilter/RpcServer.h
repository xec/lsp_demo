#pragma once

#include <vector>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>

namespace LNF
{
    typedef boost::function<bool(const std::wstring& filepath, unsigned long pid, const std::wstring& ip, unsigned short port)> RpcConnectEvent;
    typedef boost::function<bool(const std::wstring& filepath, unsigned long pid, unsigned char* buff, long bufsize)> RpcSendEvent;
    typedef boost::function<bool(const std::wstring& filepath, unsigned long pid, unsigned char* buff, long bufsize)> RpcRecvEvent;
	typedef boost::function<bool(const std::wstring& filepath, unsigned long pid, unsigned short port)> RpcBindEvent;

    class RpcServer :
        public boost::noncopyable
    {
    public:
        RpcServer();
        ~RpcServer();

        static RpcServer* Instance();
        bool Initialize();
        void Close();

        void RegisterConnectEvent(RpcConnectEvent event);
        void RegisterSendEvent(RpcSendEvent event);
        void RegisterRecvEvent(RpcRecvEvent event);
		void RegisterBindEvent(RpcBindEvent event);

    public:
        bool OnConnectEvent(const std::wstring& filepath, unsigned long pid, const std::wstring& ip, unsigned short port);
        bool OnSendEvent(const std::wstring& filepath, unsigned long pid, unsigned char* buff, long bufsize);
        bool OnRecvEvent(const std::wstring& filepath, unsigned long pid, unsigned char* buff, long bufsize);
		bool OnBindEvent(const std::wstring& filepath, unsigned long pid, unsigned short port);

    private:
        static RpcServer* instance_;

        std::vector<RpcConnectEvent>  ConnectEvents_;
        std::vector<RpcSendEvent>     SendEvents_;
        std::vector<RpcRecvEvent>     RecvEvents_;
		std::vector<RpcBindEvent>     BindEvents_;
    };
}