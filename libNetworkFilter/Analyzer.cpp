#include "Analyzer.h"
#include "ProcessAnalyze.h"

namespace LNF
{
    Analyzer::Analyzer()
    {}


    Analyzer::~Analyzer()
    {}

    Analyzer& Analyzer::Instance()
    {
        static Analyzer _x;
        return _x;
    }

    bool Analyzer::isSafeConnect(long pid, const std::wstring& ip, short port)
    {
        //
        //  Connect Event analyze process is CUI or GUI. 
        //  port is important port such as 3389, 21, 22, 23
        //
        ProcessAnalyze process(pid);
        process.analyze();
        if (process.isConsoleProcess() && port_.isImportantPort(port)) {
            return false;
        }

        if (!filesys_.isSafeDirectory(process))
            return false;

        return true;
    }

    bool Analyzer::isSafeSend(long pid, const char* buff, const long bufsize)
    {
        if (!protocol_.isSafeDataToSend(buff, bufsize))
            return false;

        ProcessAnalyze process(pid);
        process.analyze();
        if (!filesys_.isSafeDirectory(process))
            return false;

        return true;
    }

    bool Analyzer::isSafeRecv(long pid, const char* buff, const long bufsize)
    {
        //
        // Receive event analysis buffer is dangerous protocol, such as socks5
        if (!protocol_.isSafeDataForRecv(buff, bufsize))
            return false;

        ProcessAnalyze process(pid);
        process.analyze();
        if (!filesys_.isSafeDirectory(process))
            return false;


        return true;
    }

}
