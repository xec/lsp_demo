#pragma once

#include <string>
#include <vector>
#include "ProcessAnalyze.h"

namespace LNF
{
    class FilesystemAnalyze
    {
    public:
        FilesystemAnalyze();
        ~FilesystemAnalyze();
        friend class Analyzer;

        void addDeniedDirectory(const std::wstring& directory);

    protected:
        bool isSafeDirectory(const ProcessAnalyze& process);

    private:
        std::vector<std::wstring> paths_;
    };
}

