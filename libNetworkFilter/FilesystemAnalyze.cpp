#include <boost/algorithm/string.hpp>
#include "FilesystemAnalyze.h"
#include "trace.h"

namespace LNF
{
    FilesystemAnalyze::FilesystemAnalyze()
    {}


    FilesystemAnalyze::~FilesystemAnalyze()
    {}

    bool FilesystemAnalyze::isSafeDirectory(const ProcessAnalyze& process)
    {
        for each(const std::wstring& path in paths_)
        {
            if (boost::istarts_with(process.getProcessPath(), path)) {
                DebugPrint(L"Dangerous directory: %s[%s]\n",
                           process.getProcessPath().c_str(),
                           path.c_str());
                return false;
            }
        }

        return true;
    }

    void FilesystemAnalyze::addDeniedDirectory(const std::wstring& directory)
    {
        paths_.push_back(directory);
    }

}