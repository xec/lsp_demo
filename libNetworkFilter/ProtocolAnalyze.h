#pragma once

namespace LNF
{
    class ProtocolAnalyze
    {
    public:
        ProtocolAnalyze();
        ~ProtocolAnalyze();
        friend class Analyzer;

        void setSocks5AnalysisEnable(bool bEnable);
        bool getSosks5AnalysisEnable() const;

    protected:
        bool isSafeDataForRecv(const char* buff, long bufsize);
        bool isSafeDataToSend(const char* buff, long bufsize);

    protected:
        bool IsSocks5Packet(const char* buff, long bufsize);

    private:
        bool analysis_socks5_;
    };
}
