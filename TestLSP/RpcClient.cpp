#include <Windows.h>
#include <rpc.h>
#include <iostream>
#include "..\RpcInterface\Interface_h.h"
#include "RpcClient.h"
#include "stdafx.h"

#pragma comment(lib, "rpcrt4")


RpcClient::RpcClient()
	: bindHandle_(NULL)
{}


RpcClient::~RpcClient()
{
	close();
}

void RpcClient::connect()
{
	RPC_WSTR connection = nullptr;


	RPC_STATUS status = ::RpcStringBindingCompose(NULL,
		reinterpret_cast<RPC_WSTR>(L"ncacn_np"),
		NULL,
		reinterpret_cast<RPC_WSTR>(L"\\pipe\\{AAAF0E1E-18C1-4351-9483-52D7E5076B40}"),
		NULL,
		&connection);
	if (RPC_S_OK != status) {
		DebugPrint(L"RpcStringBindingCompose status = %08X\n", status);
		goto Cleanup;
	}

	status = ::RpcBindingFromStringBindingW(connection, &bindHandle_);
	if (RPC_S_OK != status) {
		DebugPrint(L"RpcBindingFromStringBindingW status = %08X\n", status);
		goto Cleanup;
	}

Cleanup:

	if (connection) {
		::RpcStringFreeW(&connection);
	}
}

bool RpcClient::is_open()
{
	return (bindHandle_ != NULL);
}

void RpcClient::close()
{
	if (bindHandle_) {
		::RpcBindingFree(&bindHandle_);
		bindHandle_ = NULL;
	}

}

boolean RpcClient::checkConnectAction(BSTR ip, unsigned short port)
{
	boolean bRet = true;
	DebugPrint(L"connect bind handle: %08X\n", bindHandle_);
	if (!bindHandle_)
		return true;


	TCHAR filepath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, filepath, MAX_PATH);
	BSTR bstrfilepath = SysAllocString(filepath);

	RpcTryExcept{
		bRet = ::checkConnectAction(bindHandle_, bstrfilepath, GetCurrentProcessId(), ip, port);
	} RpcExcept(1) {
		DebugPrint(L"Check connect action failed\n");
		bindHandle_ = NULL;
	} RpcEndExcept;

	SysFreeString(bstrfilepath);
	return bRet;
}

boolean RpcClient::checkSendAction(char* buff, DWORD bufsize)
{
	boolean bRet = true;
	SAFEARRAYBOUND sab = { 0 };
	SAFEARRAY* psa = NULL;
	char* lockedData = NULL;

	if (!bindHandle_)
		return true;


	TCHAR filepath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, filepath, MAX_PATH);
	BSTR bstrfilepath = SysAllocString(filepath);

	sab.cElements = bufsize;
	sab.lLbound = 0;

	psa = SafeArrayCreate(VT_UI1, 1, &sab);
	if (psa) {
		HRESULT hr = SafeArrayAccessData(psa, (void**)&lockedData);

		if (SUCCEEDED(hr) && lockedData) {
			memcpy(lockedData, buff, bufsize);
			SafeArrayUnaccessData(psa);
			RpcTryExcept{
				bRet = ::checkSendAction(bindHandle_, bstrfilepath, GetCurrentProcessId(), psa);
			} RpcExcept(1) {
				DebugPrint(L"Check send action failed\n");
				bindHandle_ = NULL;
			} RpcEndExcept
		}

		SafeArrayDestroy(psa);

	}
	else {
		DebugPrint(L"Allocate SAFEARRAY failed\n");
	}

	SysFreeString(bstrfilepath);

	return bRet;
}

boolean RpcClient::checkRecvAction(char* buff, DWORD bufsize)
{
	boolean bRet = true;
	SAFEARRAYBOUND sab = { 0 };
	SAFEARRAY* psa = NULL;
	char* lockedData = NULL;

	if (!bindHandle_)
		return true;


	TCHAR filepath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, filepath, MAX_PATH);
	BSTR bstrfilepath = SysAllocString(filepath);

	sab.cElements = bufsize;
	sab.lLbound = 0;

	psa = SafeArrayCreate(VT_UI1, 1, &sab);
	if (psa) {
		HRESULT hr = SafeArrayAccessData(psa, (void**)&lockedData);

		if (SUCCEEDED(hr) && lockedData) {
			memcpy(lockedData, buff, bufsize);
			SafeArrayUnaccessData(psa);
			RpcTryExcept{
				bRet = ::checkRecvAction(bindHandle_, bstrfilepath, GetCurrentProcessId(), psa);
			} RpcExcept(1) {
				DebugPrint(L"Check send action failed\n");
				bindHandle_ = NULL;
			} RpcEndExcept
		}

		SafeArrayDestroy(psa);

	}
	else {
		DebugPrint(L"Allocate SAFEARRAY failed\n");
	}

	SysFreeString(bstrfilepath);

	return bRet;
}

boolean RpcClient::checkListenAction(unsigned short port) {
	boolean bRet = true;
	DebugPrint(L"connect bind handle: %08X\n", bindHandle_);
	if (!bindHandle_)
		return true;

	TCHAR filepath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, filepath, MAX_PATH);
	BSTR bstrfilepath = SysAllocString(filepath);

	RpcTryExcept{
		bRet = ::checkListenAction(bindHandle_, bstrfilepath, GetCurrentProcessId(),  port);
	} RpcExcept(1) {
		DebugPrint(L"Check connect action failed\n");
		bindHandle_ = NULL;
	} RpcEndExcept;

	SysFreeString(bstrfilepath);

	return bRet;
}
//unsigned long RpcClient::blocked_counter()
//{
//    ULONG blockedCount = 0;
//
//    RpcTryExcept
//        blockedCount = GetBlockedCount(bindHandle_);
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//
//    return blockedCount;
//}
//
//void RpcClient::reset()
//{
//    RpcTryExcept
//        Reset(bindHandle_);
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//}
//
//unsigned long RpcClient::analyzed_counter()
//{
//    ULONG analyzedCount = 0;
//    RpcTryExcept
//        analyzedCount = GetPassthourghCount(bindHandle_);
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//
//    return analyzedCount;
//}
//
//void RpcClient::set_enable(bool bEnable)
//{
//    RpcTryExcept
//        SetEnable(bindHandle_, (bEnable == true));
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//}
//
//bool RpcClient::is_enable()
//{
//    BOOL bEnabled = 0;
//    RpcTryExcept
//        bEnabled = GetEnable(bindHandle_);
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//
//    return (bEnabled == TRUE);
//}
//
//void RpcClient::notify_reload_db()
//{
//    RpcTryExcept
//        ReloadDb(bindHandle_);
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//}
//
//void RpcClient::get_db_version(CString& version)
//{
//    BSTR* version_ptr = new BSTR();
//    RpcTryExcept
//        if (version_ptr)
//        {
//            GetDbVersion(bindHandle_, version_ptr);
//            version = *version_ptr;
//        }
//    RpcExcept(1)
//        __dbg_printf("RPC Exception: %d\n", RpcExceptionCode());
//    RpcEndExcept;
//    if (version_ptr)
//    {
//        delete version_ptr;
//    }
//}

void __RPC_FAR* __RPC_USER midl_user_allocate(size_t len)
{
	return (malloc(len));
}

void __RPC_USER midl_user_free(void __RPC_FAR* ptr)
{
	free(ptr);
}