﻿
//#include <Winsock2.h>
#include <Ws2spi.h>
#include <tchar.h>
#include <vector>
#include "../RpcInterface/interface_h.h"
#include "RpcClient.h"
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <Windows.h>
#include "stdafx.h"

#pragma comment(lib, "Ws2_32.lib")

WSPUPCALLTABLE g_pUpCallTable;           // 上层函数列表。如果LSP创建了自己的伪句柄，才使用这个函数列表
WSPPROC_TABLE  g_NextProcTable;          // 下层函数列表
TCHAR          g_szCurrentApp[MAX_PATH]; // 当前调用本DLL的程序的名称
RpcClient      rpc_client_;


LPWSAPROTOCOL_INFOW GetProvider(LPINT lpnTotalProtocols)
{
    DWORD dwSize = 0;
    int nError;
    LPWSAPROTOCOL_INFOW pProtoInfo = NULL;

    // 取得需要的长度
    if (::WSCEnumProtocols(NULL, pProtoInfo, &dwSize, &nError) == SOCKET_ERROR) {
        if (nError != WSAENOBUFS)
            return NULL;
    }

    pProtoInfo = (LPWSAPROTOCOL_INFOW)::GlobalAlloc(GPTR, dwSize);
    *lpnTotalProtocols = ::WSCEnumProtocols(NULL, pProtoInfo, &dwSize, &nError);
    return pProtoInfo;
}

void FreeProvider(LPWSAPROTOCOL_INFOW pProtoInfo)
{
    ::GlobalFree(pProtoInfo);
}

void Stop()
{
    rpc_client_.close();

    DebugPrint(L"Stop ok\n");
}

int WSPAPI WSPConnect(
    _In_   SOCKET s,
    _In_   const struct sockaddr FAR* name,
    _In_   int namelen,
    _In_   LPWSABUF lpCallerData,
    _Out_  LPWSABUF lpCalleeData,
    _In_   LPQOS lpSQOS,
    _In_   LPQOS lpGQOS,
    _Out_  LPINT lpErrno
)
{
    int ret = SOCKET_ERROR;
    DebugPrint(L"Connect event\n");

    if (namelen == sizeof(sockaddr_in)) {
        wchar_t wip[128] = { 0 };
        sockaddr_in* in = (sockaddr_in*)name;
        char* ip = inet_ntoa(in->sin_addr);
        short port = ntohs(in->sin_port);

        MultiByteToWideChar(CP_ACP, 0, ip, -1, wip, 128);
        BSTR bstrIP = SysAllocString(wip);
        DebugPrint(L"try connect %s\n", wip);
        if (!rpc_client_.checkConnectAction(bstrIP, port)) {
            DebugPrint(L"resfuse connect\n");
            *lpErrno = WSAEFAULT;
            SysFreeString(bstrIP);
            ret = WSAEFAULT;
            goto Cleanup;
        }

        SysFreeString(bstrIP);
    }


    ret = g_NextProcTable.lpWSPConnect(
              s, name, namelen,
              lpCallerData, lpCalleeData, lpSQOS, lpGQOS, lpErrno);
Cleanup:

    return ret;
}

int WSPAPI WSPSend(
    _In_   SOCKET s,
    _In_   LPWSABUF lpBuffers,
    _In_   DWORD dwBufferCount,
    _Out_  LPDWORD lpNumberOfBytesSent,
    _In_   DWORD dwFlags,
    _In_   LPWSAOVERLAPPED lpOverlapped,
    _In_   LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine,
    _In_   LPWSATHREADID lpThreadId,
    _Out_  LPINT lpErrno
)
{
    int ret = WSAECONNRESET;
    ULONG SendBufferSize = 0;
    char* buff = NULL;
    DWORD offset = 0;

    for (DWORD Index = 0; Index < dwBufferCount; Index++) {
        SendBufferSize += lpBuffers[Index].len;
    }

    if (SendBufferSize > 0 && SendBufferSize < 8192) {
        buff = new char[SendBufferSize];

        for (DWORD Index = 0; Index < dwBufferCount; Index++) {
            memcpy(buff + offset, lpBuffers[Index].buf, lpBuffers[Index].len);
            offset += lpBuffers[Index].len;
        }

        if (!rpc_client_.checkSendAction(buff, offset)) {
            DebugPrint(L"resfuse send\n");

            g_NextProcTable.lpWSPCloseSocket(s, lpErrno);
            *lpErrno = WSAEFAULT;
            *lpNumberOfBytesSent = 0;
            ret = WSAEFAULT;
            goto cleanup;
        }

    } else {
        DebugPrint(L"Invalid send size: %d\n", SendBufferSize);
    }


    ret = g_NextProcTable.lpWSPSend(
              s, lpBuffers, dwBufferCount, lpNumberOfBytesSent, dwFlags,
              lpOverlapped, lpCompletionRoutine, lpThreadId, lpErrno);
cleanup:
    if (buff) {
        delete[] buff;
        buff = NULL;
    }
    return ret;

}

void CALLBACK OverlappedCompletionRoutine(
    IN DWORD dwError,
    IN DWORD cbTransferred,
    IN LPWSAOVERLAPPED lpOverlapped,
    IN DWORD dwFlags
)
{
    DebugPrint(L"called\n");
}

int WSPAPI WSPRecv(
    SOCKET          s,
    LPWSABUF        lpBuffers,
    DWORD           dwBufferCount,
    LPDWORD         lpNumberOfBytesRecvd,
    LPDWORD         lpFlags,
    LPWSAOVERLAPPED lpOverlapped,
    LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine,
    LPWSATHREADID   lpThreadId,
    LPINT           lpErrno
)
{
    UCHAR* buff = NULL;
    HANDLE oldEvent = NULL;

    // Overlapped IO
    //if (lpOverlapped) {
    //    HANDLE hOldEvent = NULL;
    //    DebugPrint(L"iRet = %d, Error = %d\n", iRet, *lpErrno);
    //    hOldEvent = lpOverlapped->hEvent;
    //    HANDLE replaceEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    //    lpOverlapped->hEvent = replaceEvent;
    //}


    DebugPrint(L"Receive %d %08X - %08X\n", s, lpOverlapped, lpCompletionRoutine);
    int iRet = g_NextProcTable.lpWSPRecv(s, lpBuffers, dwBufferCount, lpNumberOfBytesRecvd,
                                         lpFlags, lpOverlapped, lpCompletionRoutine, lpThreadId, lpErrno);

    if (lpOverlapped == NULL && lpCompletionRoutine == NULL && iRet == 0 && *lpNumberOfBytesRecvd > 0) {
        // Synchronize read data.
        buff = new UCHAR[*lpNumberOfBytesRecvd];
        memset(buff, 0, *lpNumberOfBytesRecvd);

        if (dwBufferCount != 1) {
            DebugPrint(L"!!!!!!!! buffer count != 1(%d) !!!!!!!!!", dwBufferCount);
        } else {
            memcpy(buff, lpBuffers[0].buf,
                   *lpNumberOfBytesRecvd > lpBuffers[0].len ? lpBuffers[0].len : *lpNumberOfBytesRecvd);

            if (!rpc_client_.checkRecvAction((char*)buff, *lpNumberOfBytesRecvd)) {
                DebugPrint(L"resfuse recv\n");

                g_NextProcTable.lpWSPCloseSocket(s, lpErrno);
                *lpErrno = WSAEFAULT;
                *lpNumberOfBytesRecvd = 0;
                iRet = WSAEFAULT;
                goto cleanup;
            }
        }
        goto cleanup;
    }

cleanup:
    if (buff) {
        delete[] buff;
        buff = NULL;
    }
    return iRet;
}

int WSPAPI WSPCleanup(
    _Out_  LPINT lpErrno
)
{
    DebugPrint(L"cleanup");
    Stop();
    return g_NextProcTable.lpWSPCleanup(lpErrno);
}

int WSPAPI WSPBind(
	_In_ SOCKET s,
	_In_reads_bytes_(namelen) const struct sockaddr FAR * name,
	_In_ int namelen,
	_Out_ LPINT lpErrno
	)
{
	if (namelen == sizeof(sockaddr_in)) {
		sockaddr_in *addr = (sockaddr_in*)name;
		DebugPrint("LSP Bind local address %d\n", ntohs(addr->sin_port));

		if (!rpc_client_.checkListenAction(ntohs(addr->sin_port))) {
			DebugPrint(L"resfuse bind local port\n");

			*lpErrno = WSAEFAULT;
			return WSAEFAULT;
		}

		/*if (ntohs(addr->sin_port) == 1234) {
			*lpErrno = WSAEFAULT;
		
			return WSAEFAULT;
		}*/
	}
	return g_NextProcTable.lpWSPBind(s, name, namelen, lpErrno);
}

int WSPAPI WSPStartup(
    WORD wVersionRequested,
    LPWSPDATA lpWSPData,
    LPWSAPROTOCOL_INFO lpProtocolInfo,
    WSPUPCALLTABLE UpcallTable,
    LPWSPPROC_TABLE lpProcTable
)
{
    DebugPrint(L"  WSPStartup...  %s \n", g_szCurrentApp);

    if (lpProtocolInfo->ProtocolChain.ChainLen <= 1) {
        return WSAEPROVIDERFAILEDINIT;
    }

    // 保存向上调用的函数表指针（这里我们不使用它）
    g_pUpCallTable = UpcallTable;

    // 枚举协议，找到下层协议的WSAPROTOCOL_INFOW结构
    WSAPROTOCOL_INFOW   NextProtocolInfo;
    int nTotalProtos, i;
    LPWSAPROTOCOL_INFOW pProtoInfo = GetProvider(&nTotalProtos);

    // 下层入口ID
    DWORD dwBaseEntryId = lpProtocolInfo->ProtocolChain.ChainEntries[1];
    for (i = 0; i < nTotalProtos; i++) {
        if (pProtoInfo[i].dwCatalogEntryId == dwBaseEntryId) {
            memcpy(&NextProtocolInfo, &pProtoInfo[i], sizeof(NextProtocolInfo));
            break;
        }
    }
    if (i >= nTotalProtos) {
        DebugPrint(L" WSPStartup:	Can not find underlying protocol \n");
        return WSAEPROVIDERFAILEDINIT;
    }

    // 加载下层协议的DLL
    int nError;
    TCHAR szBaseProviderDll[MAX_PATH];
    int nLen = MAX_PATH;

    // 取得下层提供程序DLL路径
    if (::WSCGetProviderPath(&NextProtocolInfo.ProviderId, szBaseProviderDll, &nLen, &nError) == SOCKET_ERROR) {
        DebugPrint(L" WSPStartup: WSCGetProviderPath() failed %d \n", nError);
        return WSAEPROVIDERFAILEDINIT;
    }
    if (!::ExpandEnvironmentStrings(szBaseProviderDll, szBaseProviderDll, MAX_PATH)) {
        DebugPrint(L" WSPStartup:  ExpandEnvironmentStrings() failed %d \n", ::GetLastError());
        return WSAEPROVIDERFAILEDINIT;
    }

    // 加载下层提供程序
    HMODULE hModule = ::LoadLibrary(szBaseProviderDll);
    if (hModule == NULL) {
        DebugPrint(L" WSPStartup:  LoadLibrary() failed %d \n", ::GetLastError());
        return WSAEPROVIDERFAILEDINIT;
    }

    // 导入下层提供程序的WSPStartup函数
    LPWSPSTARTUP  pfnWSPStartup = NULL;
    pfnWSPStartup = (LPWSPSTARTUP)::GetProcAddress(hModule, "WSPStartup");
    if (pfnWSPStartup == NULL) {
        DebugPrint(L" WSPStartup:  GetProcAddress() failed %d \n", ::GetLastError());
        return WSAEPROVIDERFAILEDINIT;
    }

    // 调用下层提供程序的WSPStartup函数
    LPWSAPROTOCOL_INFOW pInfo = lpProtocolInfo;
    if (NextProtocolInfo.ProtocolChain.ChainLen == BASE_PROTOCOL) {
        pInfo = &NextProtocolInfo;
    }

    int nRet = pfnWSPStartup(wVersionRequested, lpWSPData, pInfo, UpcallTable, lpProcTable);
    if (nRet != ERROR_SUCCESS) {
        DebugPrint(L" WSPStartup:  underlying provider's WSPStartup() failed %d \n", nRet);
        return nRet;
    }

    // 保存下层提供者的函数表
    g_NextProcTable = *lpProcTable;

    rpc_client_.connect();

    // 修改传递给上层的函数表，Hook感兴趣的函数，这里做为示例，仅Hook了WSPSendTo函数
    // 您还可以Hook其它函数，如WSPSocket、WSPCloseSocket、WSPConnect等
    //  lpProcTable->lpWSPSendTo = WSPSendTo;
    //  lpProcTable->lpWSPRecvFrom = WSPRecvFrom;
    //  lpProcTable->lpWSPSend = WSPSend;
    lpProcTable->lpWSPRecv = WSPRecv;
    lpProcTable->lpWSPConnect = WSPConnect;
    lpProcTable->lpWSPSend = WSPSend;
    lpProcTable->lpWSPCleanup = WSPCleanup;
	lpProcTable->lpWSPBind = WSPBind;

    FreeProvider(pProtoInfo);


    return nRet;
}
