#pragma once
#include <rpc.h>
#include <atlstr.h>

class RpcClient
{
public:
    RpcClient();
    ~RpcClient();

    void connect();
    void close();
    bool is_open();

    //unsigned long blocked_counter();
    //unsigned long analyzed_counter();
    //void reset();
    //void set_enable(bool bEnable);
    //bool is_enable();
    //void notify_reload_db();
    //void get_db_version(CString& version);
    boolean checkConnectAction(BSTR ip, unsigned short port);
    boolean checkSendAction(char* buff, DWORD bufsize);
    boolean checkRecvAction(char* buff, DWORD bufsize);
	boolean checkListenAction(unsigned short port);

private:
    RPC_BINDING_HANDLE bindHandle_;
};

