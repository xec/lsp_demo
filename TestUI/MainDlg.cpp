﻿// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "aboutdlg.h"
#include "MainDlg.h"
#include <boost/format.hpp>
#include "../libNetworkFilter/RpcServer.h"
#include "../libNetworkFilter/TrustList.h"
#include "../libNetworkFilter/Analyzer.h"
//#include "../libNetworkFilter/ProcessAnalyze.h"

#pragma comment(lib, "ws2_32.lib")

BOOL CMainDlg::PreTranslateMessage(MSG* pMsg)
{
    return CWindow::IsDialogMessage(pMsg);
}

BOOL CMainDlg::OnIdle()
{
    UIUpdateChildWindows();
    return FALSE;
}

LRESULT CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    // center the dialog on the screen
    CenterWindow();

    // set icons
    HICON hIcon = AtlLoadIconImage(IDR_MAINFRAME, LR_DEFAULTCOLOR, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON));
    SetIcon(hIcon, TRUE);
    HICON hIconSmall = AtlLoadIconImage(IDR_MAINFRAME, LR_DEFAULTCOLOR, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON));
    SetIcon(hIconSmall, FALSE);

    // register object for message filtering and idle updates
    CMessageLoop* pLoop = _Module.GetMessageLoop();
    ATLASSERT(pLoop != NULL);
    pLoop->AddMessageFilter(this);
    pLoop->AddIdleHandler(this);

    UIAddChildWindowContainer(m_hWnd);

    // << [1]
    LNF::RpcServer::Instance()->Initialize();
    LNF::RpcServer::Instance()->RegisterConnectEvent(boost::bind(&CMainDlg::OnConnectEvent, this, _1, _2, _3, _4));
    LNF::RpcServer::Instance()->RegisterSendEvent(boost::bind(&CMainDlg::OnSendEvent, this, _1, _2, _3, _4));
    LNF::RpcServer::Instance()->RegisterRecvEvent(boost::bind(&CMainDlg::OnRecvEvent, this, _1, _2, _3, _4));
	LNF::RpcServer::Instance()->RegisterBindEvent(boost::bind(&CMainDlg::OnBindEvent, this, _1, _2, _3));

    // << [2]
    LNF::Analyzer::Instance().port().loadDefaultImportantPort();
    LNF::Analyzer::Instance().protocol().setSocks5AnalysisEnable(true);
    LNF::Analyzer::Instance().filesystem().addDeniedDirectory(L"C:\\Users\\Kevin\\");

    return TRUE;
}

LRESULT CMainDlg::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    // unregister message filtering and idle updates
    CMessageLoop* pLoop = _Module.GetMessageLoop();
    ATLASSERT(pLoop != NULL);
    pLoop->RemoveMessageFilter(this);
    pLoop->RemoveIdleHandler(this);

    return 0;
}

LRESULT CMainDlg::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    CAboutDlg dlg;
    dlg.DoModal();
    return 0;
}

LRESULT CMainDlg::OnOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
   /* LNF::ProcessAnalyze process(1020);

    process.analyze();*/
    
    return 0;
}

LRESULT CMainDlg::OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    CloseDialog(wID);
    return 0;
}

void CMainDlg::CloseDialog(int nVal)
{
    // << [3]
    LNF::RpcServer::Instance()->Close();
    DestroyWindow();
    ::PostQuitMessage(nVal);
}

bool CMainDlg::OnConnectEvent(const std::wstring & filepath, ULONG PID, const std::wstring& ip, USHORT port)
{
	DebugPrint(L"OnConnectEvent[%d] %s:%d\n", PID, ip.c_str(), port);

	// << [4]
	//if (!LNF::Analyzer::Instance().isSafeConnect(PID, ip, port)) {
	//	DebugPrint(L"refuse connect action\n");
	//	return false;
	//}

	std::wstring Message = boost::str(boost::wformat(_TEXT("进程 %s 尝试发起连接 %s:%d")) % filepath % ip % port);
	int ret = MessageBox(Message.c_str(), _T("警告"), MB_YESNO | MB_ICONWARNING);


	return ret == IDYES;
}

bool CMainDlg::OnSendEvent(const std::wstring& filepath, ULONG PID, unsigned char* buff, long bufsize)
{
    DebugPrint(L"OnSendEvent[%d] %d bytes\n", PID, bufsize);
  /*  
    if (!LNF::Analyzer::Instance().isSafeSend(PID, (const char*)buff, bufsize)) {
        return false;
    }*/
    return true;
}

bool CMainDlg::OnRecvEvent(const std::wstring& filepath, ULONG PID, unsigned char* buff, long bufsize)
{
    DebugPrint(L"Recv event\n");
  
    if (!LNF::Analyzer::Instance().isSafeRecv(PID, (const char*)buff, bufsize)) {
        return false;
    }

    return true;
}

bool CMainDlg::OnBindEvent(const std::wstring & filepath, ULONG PID, USHORT port)
{
	DebugPrint(L"Bind event\n");

	std::wstring Message = boost::str(boost::wformat(_TEXT("进程 %s 尝试监听端口 %d")) % filepath % port);
	int ret = MessageBox(Message.c_str(), _T("警告"), MB_YESNO | MB_ICONWARNING);
	return ret == IDYES;
}
