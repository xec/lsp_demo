// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//  are changed infrequently
//

#pragma once

// Change these values to use different versions
#define WINVER		0x0500
#define _WIN32_WINNT	0x0501
#define _WIN32_IE	0x0501
#define _RICHEDIT_VER	0x0500

#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>

#if defined _M_IX86
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#ifdef _DEBUG

static void DbgPrintA(const char * format, ...)
{
    //#ifdef _DEBUG
    char buff[4096] = { 0 };
    va_list ap;
    va_start(ap, format);
    _vsnprintf_s(buff, _countof(buff), format, ap);
    va_end(ap);

    OutputDebugStringA(buff);
    //std::cout << buff;
}

static void DbgPrintW(const wchar_t * format, ...)
{
    //#ifdef _DEBUG
    wchar_t buff[4096] = { 0 };
    va_list ap;
    va_start(ap, format);
    _vsnwprintf_s(buff, _countof(buff), format, ap);
    va_end(ap);

    OutputDebugStringW(buff);
    //std::wcout << buff;
}

#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) L ## #x
#define LINE_STRING STRINGIZE(__LINE__)
#define STR(x) STR2(x)
#define STR2(x) L ## x
#define FUNC STR(__FUNCTION__)
#define PrintPrefix L"[LSP][" FUNC L":" LINE_STRING L"] "

#endif

#ifdef _DEBUG

#define DebugPrint(...) DbgPrintW(PrintPrefix __VA_ARGS__)
#else

#define DebugPrint(...)

#endif